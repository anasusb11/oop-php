<?php 

require_once 'Ape.php';
require_once 'Frog.php';
$sheep = new Animal("shaun");

echo " Name: " . $sheep->name; // "shaun"
echo "<br>";
echo " Legs : ". $sheep->legs; // 4
echo "<br>";
echo " Cold Blooded : ".$sheep->cold_blooded; // "no"
echo "<br>";
echo "<br>";


//ini pakai METHOD GET
// echo $sheep -> get_name(); 
// echo "<br>";
// echo $sheep -> get_legs(); 
// echo "<br>";
// echo $sheep-> get_cold_blooded();


$sungokong = new Ape("kera sakti");
echo " Name: " . $sungokong->name; // "shaun"
echo "<br>";
echo " Legs : ". $sungokong->legs; // 4
echo "<br>";
echo " Cold Blooded : ".$sungokong->cold_blooded; // "no"
echo "<br> Yell : ";
echo $sungokong->yell();// "Auooo"
echo "<br>";
echo "<br>";



$kodok = new Frog("buduk");

echo " Name: " . $kodok->name; // "shaun"
echo "<br>";
echo " Legs : ". $kodok->legs; // 4
echo "<br>";
echo " Cold Blooded : ".$kodok->cold_blooded; // "no"
echo "<br> Jump : ";
echo $kodok->jump();


?>
